﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TutorialForInterface
{
    class Program
    {
        static void Main(string[] args)
        {
            // The new parameters are created and the constructor is called below:
            int firstParameter = 5;
            int secondParameter = 10;
            Rectangle MyRectangle = new Rectangle(firstParameter, secondParameter);
            Console.WriteLine("My rectangle has been created.");
            // The size of the rectangle is received via properties 
            int gotFirstParameter = MyRectangle.sideA;
            int gotSecondParameter = MyRectangle.sideB;
            Console.WriteLine("Size of my rectangle: A = {0} B = {1}", gotFirstParameter, gotSecondParameter);
            // The area of the rectangle is received via method
            int calculatedArea = MyRectangle.Area();
            Console.WriteLine("Area of my rectangle: {0}", calculatedArea);
            // The perimeter of the rectangle is received via method and written to the Console without saving the value
            Console.WriteLine("Perimeter of my rectangle: {0}", MyRectangle.Perimeter());

            // The setters of sideA and sideB property are not accessible because of 'private set' (below)
            //MyRectangle.sideA = 6;
            // The HalfPerimeter() method cannot be called outside of the class (below), because it's private
            //MyRectangle.HalfPerimeter();
        }
    }
}
