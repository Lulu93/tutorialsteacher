﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TutorialForInheritance
{
    class Square : Rectangle
    {

        public Square(int a) : base(a,a)
        {
        }

        public override string Name
        {
            get { return "SQUARE"; }
        }
    }
}
