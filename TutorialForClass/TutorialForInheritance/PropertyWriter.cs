﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TutorialForInheritance
{
    class PropertyWriter
    {
        public void WritePerimeterAndAreaOfPolygon(IPolygon actualPolygon)
        {
            Console.WriteLine("Type of polygon: {0}", actualPolygon.Name);
            Console.WriteLine("Area of my polygon: {0}", actualPolygon.Area());
            Console.WriteLine("Perimeter of my polygon: {0}", actualPolygon.Perimeter());
        }
    }
}
