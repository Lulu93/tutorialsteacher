﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TutorialForInterface
{
    class Program
    {
        static void Main(string[] args)
        {
            Rectangle X = new Rectangle(4,5);
            PropertyWriter writer = new PropertyWriter();
            writer.WritePerimeterAndAreaOfPolygon(X);
            Console.ReadLine();
        }
    }
}
